template <typename BaseTy, typename UniqueID> struct TypeWrapper {
  BaseTy a;

  template <typename OtherID>
  TypeWrapper<BaseTy, UniqueID>
  constexpr operator=(const TypeWrapper<BaseTy, OtherID> &rhs) noexcept {
    a = rhs.a;
    return *this;
  }
};

template <typename PtrTy1, typename PtrTy2>
void copy(PtrTy1 *input, PtrTy2 *output) {
#pragma unroll
  for (int i = 0; i < 4; i++)
    output[i] = input[i];
}

// Vectorization happens when BaseTy = int.
// Vectorization does not happen when BaseTy = char.
using BaseTy = char;
using Wrapper1 = TypeWrapper<BaseTy, class id1>;
using Wrapper2 = TypeWrapper<BaseTy, class id2>;

void copy_non_overlapping(BaseTy *ptr1, BaseTy *ptr2) {
  copy(reinterpret_cast<Wrapper1 *>(ptr1), reinterpret_cast<Wrapper2 *>(ptr2));
}
