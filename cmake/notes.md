# Sources:

1. [A basic overview with
examples](https://www.youtube.com/watch?v=7W4Q-XLnMaA). 30min video,
class-style.

2. [Series of short tutorials on
youtube](https://m.youtube.com/playlist?list=PLK6MXr8gasrGmIiSuVQXpfFuE1uPT615s).
Great intro to the CMake language itself.

3. [Modern CMake practices](https://www.youtube.com/watch?v=bsXLMQ6WgIk&t=2504s).
Good talk by Daniel Pfeifer

# What is CMake.

A build system generator. It is NOT a build system.

It makes sense to have `GLOB` expressions in the build system: it evaluates
them when the build happens.

It does not make sense to have `GLOB` expressions in the build system
generator: building will not cause them to be evaluated.

# Don'ts

1. Don't use `include_directories()`.
2. Don't use `link_directories()`.
3. Don't use `set(CMAKE_CXX_FLAGS -std=c++11)`. Use `target_compiler_features`
or `set(CMAKE_CXX_STANDARD 11)`

# Why CMake

People hated writing Makefiles.

A lot of typing, not really scripting, dependency management is hard, flags, etc.

CMakes take of many different stages of building a project:

1. Configure & Generate (traditionally what CMake is known for)
2. Build
3. Test (CTest)
4. Package (CPack)

# Basic directory structure

We distinguish between:

* The *source* directory: location of `CMakeLists.txt`, sources and everything
needed to build the project. Frequently under version control.

* The *build* or *binary* directory: location of executables, libraries, test
output, packages, `CMakeCache.txt`

# In source vs out of source builds

When build files are intermixed with source files, we say that this is a _in
source_ build.  This is bad:

* impossible to use the same source files for multiple builds.
* tracking what needs to be committed by control version systems is difficult.
* directory gets polluted
* risk of build process destroying source files.

This is fixed by using an _out of source_ build, where a directory outside the
_source_ directory is created. This is the build/binary directories mentioned
previously.

# Alternative way to build

(other than calling ninja, make, etc)
In the build dir:

`cmake --build .`

This way we can always build regardless of the target used.

# Basic CMake file:
``` cmake
cmake_minimum_required(VERSION 3.12)
project(MyProject VERSION 1.0.0)

add_executable (hello_executable  main.cpp hello_impl.cpp)  #source files in the same dir as base project.
```

# Adding a library:
``` cmake
cmake_minimum_required(VERSION 3.12)
project(MyProject VERSION 1.0.0)

  add_library (
    say-hello-lib
    SHARED      #optional, default for library is STATIC.
    hello.hpp
    hello.cpp
    )

add_executable (hello_executable  main.cpp)  #source files in the same dir as base project.

target_link_libraries(hello_executable PRIVATE say-hello-lib)
```

Running `ldd` on `hello_executable` shows the dependency on the library, if it was SHARED.

If `cmake -D BUILD_SHARED_LIBS=TRUE` is used, the default is overriden.

#  Dividing project into directories.

```
build/
project/
  CMakeLists.txt

  hello_exe/
    CMakeLists.txt
    main.cpp

  say_hello_lib/
    CMakeLists.txt
    src/
      say_hello/
        hello.cpp
        hello.hpp
```

## Main CMakeLists

``` cmake
cmake_minimum_required(VERSION 3.12)
project(MyProject VERSION 1.0.0)

add_subdirectory(say_hello_lib)
add_subdirectory(hello_exe)
```

## Executable CMakeLists

``` cmake
add_executable (hello_executable  main.cpp)  #source files in the same dir as base project.

target_link_libraries(hello_executable PRIVATE say_hello_lib)
```

## Library CMakeLists
```cmake
  add_library (
    say_hello_lib
    src/say_hello/say_hello.hpp
    src/say_hello/say_hello.cpp
  )

  target_include_directories(
    say_hello_lib
    PUBLIC
    "${CMAKE_CURRENT_SOURCE_DIR}/src"
  )

  target_compile_definitions (
    say_hello_lib
    PUBLIC
    SAY_HELLO_VERSION=4
  )
```

`PUBLIC` means that both consumers and the library itself have access to
the definition.

`PRIVATE` would only expose the definition to the library.

`INTERFACE` would only expose the definition to consumers of the library.

## Inside main.cpp

```c++
#include "say_hello/hello.hpp"

cout << "Hello world lib version = " << HELLO_WORLD_LIB_VERSION << "\n";
```

# CMake scripting basics

## Comments

Comments are preceded by `#`.

## Commands structure

You cannot have two commands in the same line. E.g. this is a parsing error:

```
some_command(ARGUMENT_LIST) some_other_command(ARGUMENT_LIST2)
```

## Two basic commands

Specifying a minimunm required version is a good practice to ensure sanity of
the project.

```
cmake_minimum_required(VERSION 3.12)
```

The `project` command must be present in the top-level CMakeLists.txt, otherwise
weird things happen:

```
project(MyProject VERSION 1.0.0)
```

## Strings everywhere

In the CMake language, everything is a string. For instance, the three
arguments below are strings:

```
project(MyProject VERSION 1.0.0)
```

Doing `MyProject` or `"MyProject"` would yield the exact same result.

## Spaces and lists

```
set(var ab)                   # var = "ab"
set(var ${var}ab)             # var = "abab"
set(var var)                  # var = "var"

set(var a b c)                # var = "a;b;c"  << a list!
set(var a;b;c)                # var = "a;b;c"  << a list!
set(var a b;c)                # var = "a;b;c"  << a list!
set(var a "b c")              # var = "a;b c"  << a list!
```

## Multiline strings

```
set(var
"I use quotes \"\"
To capture many lines!"
)
```

Other characters are also escaped with a `\`.

With CMake 3.0 or higher, we can use `[=[` and `]=]` (with any number >= 0 of `=`).
This syntax disables all variable substituion and doesn't require escaping.

If the first character is a newline, it is ignored:

```
set(var [[
 I don't use quotes ""
 To capture many lines.
]])

set(shellScript [=[
#!/bin/bash
[[ -n "${USER}" ]] && echo "Have USER"
]=])
```

## Arguments

Arguments are separated by whitespaces, as many as you want.

## Printing

This is done with the `message` command:

* First argument is the message mode. Usually `STATUS`.
* Second argument is the message itself. Since messages usually contain spaces, we wrap them in quotes.

```
message(STATUS "I am a message")
```

Without any spaces, we would have printed the concatenation of the arguments, i.e. `Iamamessage`.

## Variables

Variables are created with the `SET` command.

```
set(MY_VAR "I am a var")
```

Variables are read by using `${}`.

```
message(STATUS "MY_VAR is: ${MY_VAR}")
```

Some commands define variables implicitly.

```
project(MyProject VERSION 1.0.0)
message(STATUS "My project name is: ${PROJECT_NAME}")
message(STATUS "My project name is: ${PROJECT_VERSION}")
```

## Conditional executions

They are surrounded by `if` and `endif` statements.

```
if ("TRUE") # "FALSE" would have done the opposite.
  message(STATUS "Condition met")
else()
  message(STATUS "Condition not met")
endif()
```

Note: it used to be the case that `endif()` had to repeat the condition in the
`if()` clause. However, this is no longer necessary and considered bad
practice.

When using a variable inside an `if` condition, it is not necessary to dereference it.

```
set(some_bool FALSE)
```

```
if(some_bool)
if("${some_bool}") # both do the same.
```

Negations are done using `NOT`:

```
if(NOT some_bool)
```

Strings can be compared too.

```
set(STRING1 "one string")
set(STRING2 "another string")

if(STRING1 STREQUAL STRING2)
```

Careful with string names:

```
set(STRING1 "one string")
set(STRING2 "another string")

if(STRING1 STREQUAL "STRING2") # This is false! Because we are comparing
                               # against a string literal, not the variable
```

It's possible to pattern match strings, compare integers, etc.
(videos have more information on this)

It's also possible to do basic arithmetic with variables.

## Control flow.

There are `while` loops, but the most form of loop is with a `foreach` command.

```
foreach(item IN ITEMS foo bar baz)
  message(STATUS "Item is: ${item}")
endforeach()
foreach(idx RANGE 100)
  message(STATUS "idx is: ${idx}")
endforeach()
```


# Functions

## Argument passing

```
set_property(
  GLOBAL    # This could have been "GLOBAL", nothing changes.
  PROPERTY
  FOO
  1
  2
  3
)

get_cmake_property(foo_value FOO)
message(STATUS "Value of FOO is ${foo_value}")
```

All of the paraters to `set_property` are arbitrary strings. It's up to the
function implementation to interpret them.

This program prints `1;2;3`.

To see how all arguments are just plain strings, look at the equivalent program:

```
set(number_list 1 2 3)
set(target "GLOBAL")  #could have been GLOBAL with no quotes

set_property(
  "${target}"
  PROPERTY
  FOO
  "${number_list}"
)

get_cmake_property(foo_value FOO)
message(STATUS "Value of FOO is ${foo_value}")
```

## When to dereference a variable with quotes?

If we use `"${target}"`, CMake expands this to a *single argument*.
If we use `${target}`, CMake expands this to a *single argument*.

```
set(number_list 1 2 3)
set(target "DIRECTORY ${PROJECT_SOURCE_DIR}")  #could have been GLOBAL with no quotes

set_property(
  "${target}"
  PROPERTY
  FOO
  "${number_list}"
)

get_cmake_property(foo_value "${target}" FOO)
message(STATUS "Value of FOO is ${foo_value}")
```

This won't compile.

Set property expects one argument `DIRECTORY` followed by
another argument with some path.

But we gave it a single argument that is the `DIRECTORY;current/path`.

To fix this example, we need to remove the quotes!

```
set(number_list 1 2 3)
set(target "DIRECTORY ${PROJECT_SOURCE_DIR}")  #could have been GLOBAL with no quotes

set_property(
  "${target}"
  PROPERTY
  FOO
  "${number_list}"
)

get_cmake_property(foo_value "${target}" FOO)
message(STATUS "Value of FOO is ${foo_value}")
```

## Defining custom functions

```
function(foo_name)
  message(STATUS "you called me")
endfunction()

foo_name()
foo_name()
```

Arguments are just extra parameters to the `function` function.

```
function(foo_name first_arg)
  message(STATUS "you called me with arg: ${first_arg}")
endfunction()

foo_name(1)
foo_name(1, 2) # this is also fine!
```

## Variadic functions

```
function(foo_name non_var_arg)
  foreach(arg IN LISTS ARGN)
    message(STATUS "variadic arg argn:: ${arg}")
  endforeach()
  foreach(arg IN LISTS ARGV)
    message(STATUS "all args argv: ${arg}")
  endforeach()
endfunction()

foo_name(1, 2)
```

## Scope of variables and function calls

When a function is called, CMake *copies* the entire scope available at
the call site and makes it the scope of the callee.

```
set(GLOBAL_VAR_DEFINED_BEFORE "mydefbefore")

function(foo_name)
  message(STATUS "var defined before = ${GLOBAL_VAR_DEFINED_BEFORE}")
  message(STATUS "var defined after = ${GLOBAL_VAR_DEFINED_AFTER}")

  set(GLOBAL_VAR_DEFINED_BEFORE "updated_def_before")

  message(STATUS "updated var defined before = ${GLOBAL_VAR_DEFINED_BEFORE}")
endfunction()

set(GLOBAL_VAR_DEFINED_AFTER "mydefafter")

foo_name()

message(STATUS "after call, updated var defined before = ${GLOBAL_VAR_DEFINED_BEFORE}")
```

This program will print:

```
mydefbefore
mydefafter
updated_def_before
mydefbefore
```

Changes to variables in the scope of the callee will not affect the scope of the caller
unless we use a special argument to the `set` function:

```
set(GLOBAL_VAR_DEFINED_BEFORE "updated_def_before" PARENT_SCOPE)
```

*However, this won't change variable in the callee scope!*

## The mindblowing argument passing problem

Recall the following function signature we had before:

`function(foo_name arg)`

When we called with `foo_name(1)` and printed `${arg}`, we got `1`.

Simple, right? We pass *the string* `1` as argument, and that binds `$arg` to
that string.

But this confuses everybody:

```
function(foo_name arg)
  message(STATUS "arg = ${arg}")
endfunction()

set(value 14)
foo(value)
```

What will it print? It's not 14!

The callsite will bind `${arg}` to `value`. That is, dereferencing `arg` will
produce the *string* `value`.

If we want to get the variable represented by that name, we need to dereference
it again!

```
function(foo_name arg)
  message(STATUS "arg = ${${arg}}")
endfunction()
```

## Returning a value with the help of double indrections

It's this double indirection mechanism that allows for values to be returned.

```
function(increment arg)
  math(EXPR arg_plus_one "${${arg}} + 1"
  set("${arg}" "${arg_plus_one}" PARENT_SCOPE)
endfunction()

set(value 14)
increment(value)
message(STATUS "value after increment = ${value}"
```

## `cmake_parse_arguments`

That's one of the most powerful features of CMake scripting.

It enables the idea of named arguments.

```
# cmake -Hexamples/parse_arguments -Bexamples/parse_arguments/build

function(foo)
  set(options PRINT_NAME_TWICE)
  set(args NAME NICKNAME NUMBER NOBODY_USES_THIS)
  set(list_args FOODS)

  cmake_parse_arguments(
    PARSE_ARGV 0 # should we skip any arguments?
    foo_arg      # prefix of the generated variables
    "${options}"
    "${args}"
    "${list_args}"
  )

  message(STATUS "called foo")
  message(STATUS "parsed name = ${foo_arg_NAME}")
  if(${foo_arg_PRINT_NAME_TWICE})
    message(STATUS "[printing twice] parsed name = ${foo_arg_NAME}")
  endif()
  message(STATUS "parsed nickname = ${foo_arg_NICKNAME}")
  message(STATUS "parsed number = ${foo_arg_NUMBER}")
  message(STATUS "parsed foods = ${foo_arg_FOODS}")
  foreach(arg IN LISTS foo_arg_UNPARSED_ARGUMENTS)
    message(WARNING "Unparsed argument: ${arg}")
  endforeach()
  if(NOT DEFINED foo_arg_NOBODY_USES_THIS)
    message(STATUS "good, nobody uses this.")
  endif()
endfunction()

foo(
  some_unparsed_arg
  NAME felipe
  NICKNAME rictor
  NUMBER 17
  FOODS pasta meatballs
  PRINT_NAME_TWICE
)
```

# Some CMake variables

## `CMAKE_SOURCE_DIR` and `CMAKE_BINARY_DIR`

Those variables point to the location of the top-level `CMakeLists.txt` and to
the top level build directory.

They never change once CMake starts running.

## `CMAKE_CURRENT_LIST_FILE`  and `CMAKE_CURRENT_LIST_DIR`

Point to the currently executing `CMakeLists.txt` file/directory.

Note: if using a helper function, these variables will contain the call site,
not the helper file. This could be the helper file, if it itself calls the
helper function.

## `PROJECT_NAME`, `PROJECT_SOURCE_DIR` and `PROJECT_BINARY_DIR`

These are set by the `project` function.

Note: if we print them *before* the call to `project`, we will be printing the
values of the enclosing project context (i.e. probably the project one level
up).
