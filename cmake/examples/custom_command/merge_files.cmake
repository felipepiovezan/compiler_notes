file(STRINGS ${in1} lines_in1)
file(STRINGS ${in2} lines_in2)

if(WIN32)
  set(carriage_return "\r")
endif()

# Clear the contents of out.
file(WRITE ${out} "")

foreach(line IN ITEMS ${lines_in1})
  file(APPEND ${out} ${line} ${carriage_return} "\n")
endforeach()
foreach(line IN ITEMS ${lines_in2})
  file(APPEND ${out} ${line} ${carriage_return} "\n")
endforeach()
