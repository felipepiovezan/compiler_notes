# Call site
[_Dragon book_]

A place in the program where a procedure is invoked.

# Call Graph
[_Dragon book 12.1.1_]

Given a program `P`, the call graph of `P`, `CG(P) = {V, E}`, is a graph such
that:

* V contains one node per procedure of `P`.
* V contains one node per call site of `P`.
* If a call site `c` may call procedure `foo`, (c, foo) is an edge in `E`.

In programs without virtual function calls (or without any sort of calling
through function pointers), each call site has exactly one edge in the graph.
In other words, the target of all calls is statically determined.

# Call string
[_Dragon book 12.1.3_]

The calling context is defined by the contents of the entire call stack.
The string of call sites on the stack is the _call string_.

# SCC
[_Dragon book 12.6.1_]


Given a program `P`,  {V, E}`, consider a graph such that:

* V contains one node per procedure of `P`.
* If a procedure `p1` calls procedure `p2`, (p1, p2) is an edge in `E`.

The _strongly connected components_ of this graph are the sets of _mutually
recursive functions_.

Special case: a node (function) with just an edge to itself (that just calls itself).

An SCC is _nontrivial_ if:

1. it has more than one member (the "mutually recursive case"), or
2. it has a single member with an edge to itself ("a single recursive member").

The other SCCs in the graph are all single, non-recursive, nodes representing
non-recursive procedures.
