# Region definition
From [_Engineering a Compiler_].

* Local = a BB.
* Region = some set of BBs.
* Global = all BBs inside a function.
* Interprocedural / whole-program = all functions in the program / translation
  unit.

# Local value numbering
From [_Engineering a Compiler_].
(I'm assuming SSA form here, the book does not)

Inside a BB, hash triples of (value, op, value) to a unique number. This allows
the compiler to find values being computed multiple times.

# Superlocal value numbering
From [_Engineering a Compiler_].
(I'm assuming SSA form here, the book does not)

Instead of considering BBs individually, compute _extended basic blocks_. I.e.
consider all BB paths in the CFG such that all BBs in the path have a single
single predecessor. Pretend those BBs are in a single BB and perform local
value numebering on them.

(_Why does it work? Because in this extended basic block, there will never be phi
nodes in the middle of the instruction stream! Notice: if we assume it's
invalid to have phi nodes in the middle of a BB, our extended BBs are defined
such that they will respect that definition._)

# Loop unrolling
From [_Engineering a Compiler_].

Consider two loops that are nested. Unrolling can be done either on the outer
loop, the inner loop, or both. In either case, it might be necessary to have a
prologue loop that peels off up to `num_iterations % unroll_factor`.

When unrolling in done in the outer loop, it will produce some number of inner
loop copies.  Those copies may be fused in what is called _loop fusion_. Outer
loop unrolling followed by inner loop fusion is also known as _unroll and jam_.

Some points to consider, about unrolling:

* Reduces the number of control flow instructions executed.
* May create opportunities for reuse of registers across iterations.
  Probably through value numbering.
* Increases program size. If it increases the loop size so that it is greater
  than the instruction cache, this impact will overwhelm any positive gains
  from unrolling.
* Exposes more instructions to the instruction scheduler, potentially more ILP.
* Exposes consecutive memory acceses, enabling memory coalescing and or locality.
* Increases register pressure.

# Live variables
From [_Engineering a Compiler_].
(I'm assuming SSA form here, the book does not)

A variable is said to be alive a point in the program if there exists a path
from that point to a use of the variable, such that the variable is not
redefined in the path.

One could argue that the redefinition condition is not useful when using SSA form.
Notice, however, that redefinition may happen on paths with phi nodes.

Live-variable information is useful during register allocation: the allocator
needs to keep a value in register for as long as the value is live.

Live-variable information is used to improve SSA construction: if the value is
not alive in a block, it does not need a phi node in that block.

# Global code placement
From [_Engineering a Compiler_].

The fallthrough path of a branch is usually the cheapest path:
cache-friendly. Code placement willtry to put the most frequently taken
branches in the fallthrough path and move infrequently used blocks to the end
of the procedure.

Profiling is useful to obtain information about which CFG edges are taken more
often. With this, we can find _hot paths_ in the code.

A greedy algorithm can be used:
  0. `current_priority = 0`
  1. Initialize each node `v` to be in a path by itself, with `cost(path(v)) = +inf`
  2. Visit each edge `(x, y)`, sorted by hottest edge first.
    3. If `x` is the last node of its path and `y` is the first node of its
    path:
      4. Merge the two paths, set `cost(path(x)) = min(cost(path(x),
      cost(path(y)), current_priority))`
      5. `current_priority++`

(lower cost means hotter)

This imposes a partial ordering among BBs. The compiler must convert that
ordering into a total ordering, i.e., make a sequence of BBs.

Place the chain related to the entry BB as the first chain of the function.
Enqueue all chains containing successors of the placed BBs and place them by
hotness order.

# Inlining
From [_Engineering a Compiler_].

The transformation itself is simple, but choosing when to do it is where
complications arise.

One benefit from inlining is that it removes function call overhead (_which is
usually not seen at the IR level_)

However, inlining may make global (i.e. intra-procedure) optimization harder.
(_It may cause a basic block to branch earlier, change the CFG of the callee,
making many optimizations harder to perform._)

It also changes the problem that will eventually be seen by the register allocator.

Other considerations:

* Callee size: often the callee is smaller than the function call overhead
  (pre-call, post-return, prolog, epilog).
* Caller size: avoid making the caller too big.
* Dynamic call count: call sites executing often are more important to be
  optimized.
* Cosntant-valued actual parameters: if parameters are known in compile time,
  inlining might create more optimization opportunities for that call site.
* Static call count: if a procedure has a single call site, it is safe to
  inline it without increasing code size! (_The compiler still needs to
  consider the downsides, like preventing intra-procedure optimizations_)
* Parameter count: the more parameters, the more expensive the call is.
* Call made by a procedure: leaves in the call graph are good candidate for
  inlining.

# Procedure placing
From [_Engineering a Compiler_].

Given the call graph of a program and an estimated execution frequency of each
call site, find a total ordering among procedures that minimizes virtual-memory
working-set sizes and that minimizes call-induced instruction cache conflicts.

A simple algorithm is given: sort edges by their weights, repeatedly fuse nodes
starting from the highest weight edge. When nodes are fused, update the other
edges to point to the new fused node or to be pointed by the fused node. Do not
update weights.

The fused node represents a list with the order that procedures are
to be placed.


# Suggested books

1. Optmizing compilers  for modern architectures. J R Allen, K Kennedy,
2. Building an optimizing compiler. R Morgan.
3. Muchnick

(those have been suggested as books on optimizations).
