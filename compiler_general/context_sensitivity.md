# Context sensitivity
[_Dragon book 12.1.2_]

The behavior of a procedure is dependent on the context in which it is called.

# Context insensitive analysis
[_Dragon book 12.1.2_]

Treat each call and return operations as a branch in the CFG, as if a single
procedure existed. Create a _super control flow graph_ where, besides the
normal CFG graph, we include:

1. An edge from the call site to the entry block of the procedure it calls.
2. An edge from the return statement back to the call site. (_we would probably
need to split the basic block at the call site_)

This is not very useful, as it abstracts our the relationship between input and
output values in procedure invocations.

Example:

``` c++
for (int i = 0; i < n; i++) {
  f(0);
  f(10);
  f(10);
}
```

```
                                   +-------------+
                                   | i = 0       |
                                   +------+------+
+----------------------+                  |
|                      |           +------v-------+
|                      |           |loop pre header
|     latch block      +---------->+              +------------>
+--------+-------------+           +------+-------+
         ^                                 |
         |                         +-------v------+
         |                         | v = 0        |
         |                         |              +------------+
         |                         +--------------+            |
         |                         +---------------+           |
         |                         | v = 10        +---------+ |
         |                +-------->               |         | |
         |                |        +---------------+         | |
         |                |        +------------------+      | |
         |                |        |  v = 10          |      | |
         |                |        |                  |      | |
         |                |        +-----^-------+----+      | |
         |                |              |       |           | |
         |                |        +-----+-------v-----+     | |
         |                +--------+  compute f        +<----+ |
         ^-------------------------+                   +<------+
                                   +-------------------+
```

It's not possible to see, from this representation, what each individual invocation of `f` is computing.

# Context sensitive analysis
[_Dragon book 12.1.3_]

By considering the call string, or the context in which call is made, the
analysis may derive more information about the program.

When performing analysis, it's possible to consider a limited context:
keep at most `k` most immediate call sites, _k-limiting context analysis_. When
`k = 0`, we have context insensitive analysis.
